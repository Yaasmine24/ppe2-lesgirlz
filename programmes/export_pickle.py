import pickle
from datastructures import Corpus

def write_pickle(corpus: Corpus, destination: str):
    with open(destination, "wb") as file:
        pickle.dump(corpus, file)
