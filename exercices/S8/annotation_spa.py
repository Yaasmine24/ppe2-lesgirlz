import spacy

from collections import namedtuple
from dataclasses import dataclass

from datastructures import Token, Article

def nlp(): 
    return spacy.load("fr_core_news_sm")


def analyse_spac(txt):
    doc = nlp(txt)
    annotation = []
    for token in doc:
        
        totoken= token.text
        annotation.append(totoken)

        popos= token.pos_
        annotation.append(popos)

        lelemme = token.lemma_
        annotation.append(lelemme)
        
        hehead = token.head
        annotation.append(hehead)
    
        dedep = token.dep_
        annotation.append(dedep)
        annotation.append(" | ")
        
    return annotation
    

