import feedparser # tout notre fichier xml va avoir la structure d'un dico python

# Analyse du fichier XML avec feedparser
feed = feedparser.parse("sample.xml")

#Boucle avec les élément de l'arbre XML donc entries représente tous les éléments
for entry in feed['entries']: 
    # Et nous voulons juste les éléments "titre" et "descritption"
    print("titre =", entry['title'])
    print("description =", entry['description'])
