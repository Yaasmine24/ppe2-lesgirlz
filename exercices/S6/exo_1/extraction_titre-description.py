import re 
import pathlib

fic = pathlib.Path('sample.xml')
xml = fic.read_text()
l= re.findall(r'<item>.*?</item>', xml)

############
# Permet de récupèrer les éléments du titre et de les stocker
# dans la variable 'titre'.
# Puis, nettoyage des éléments non importants (exemple le nom des balises ou le [CDATA[
############
titre= []

for article in l:
    titre+=((re.findall(r"<title>.*?</title>", article)))
#print(titre)

net_titre=[]
for i in titre:
    net_titre.append(i.strip('<title>![CDATA[></'))
print(net_titre)


############
# Même principe que le premier bloc, mais cette fois-ci la description est stockée
#
############


description=[]
for descri in l:
    description+=((re.findall(r"<description>.*?</description", descri)))
#print(description)

net_description=[]
for i in description:
    net_description.append(i.strip('<description><![CDATA[ ]]></'))
print(net_description)
