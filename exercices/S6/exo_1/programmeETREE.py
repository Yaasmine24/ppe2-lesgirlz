# On importe et on renomme le module pcq c'est plus pratique
import xml.etree.ElementTree as ET 

# Analyse du fichier XML nommé <sample.xml> avec ElementTree
tree = ET.parse("sample.xml")

# Récupération de la racine de l'arbre XML, ici getroot permet de récupérer le noeud racine
root = tree.getroot()

#Parcours de notre arbre XML donc d'abord channel puis item car c'est dans item que se trouve les titres
channel = root.find('channel')

#En faisant findall, on récupère tous les élements items et pas juste le premier
items = channel.findall('item')

#Boucle for qui permet de dire que pour tous ces éléments items, on veut récuperer leur titres
for item in items: 
    title = item.find('title')
    description = item.find('description')
    #Enfin, on affiche leur titre 
    print(title.tag, '=>', title.text, '\n', description.tag,'=>', description.text )
    
